package com.casallas.repaso_rv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter <Element>{//se indica la clase view holder ya creada

    List<String> Elements; //Variable tipo lista para guardar los elementos

    //constructor para inicializar elements
    public Adapter (List<String>datos){
        Elements=datos;
    }

    @NonNull
    @Override
    public Element onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View miCajon;
        miCajon=LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento,parent,false);
        return new Element(miCajon);
    }

    @Override
    public void onBindViewHolder(@NonNull Element holder, int position) {

        String miDato=Elements.get(position);
        holder.txtElement.setText(miDato);
    }

    @Override
    public int getItemCount() {
        if (Elements!=null){
            return Elements.size();
        }else {
            return 0;
        }

    }
}

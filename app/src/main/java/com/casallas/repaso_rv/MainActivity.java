package com.casallas.repaso_rv;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvElements;
    Adapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //se tiene el RV
        rvElements=findViewById(R.id.rv_elementos);
        rvElements.setLayoutManager(new LinearLayoutManager((this)));
        //definir datos, pueden venir de muchos sitios

        //Se definen datos en un array

        List<String> misDatosLocales=new ArrayList<>();
        misDatosLocales.add("Paula");
        misDatosLocales.add("Carlos");
        misDatosLocales.add("Andrés");
        misDatosLocales.add("Jesús");
        //Crea objeto de la clase adaptador
        myAdapter=new Adapter(misDatosLocales);
        rvElements.setAdapter(myAdapter);

        getElements();
    }
    //Para llamar información desde api
    public void getElements(){
        String miUrl="https://www.balldontlie.io/api/v1/teams"; //Retorna un JSON
        RequestQueue miPila= Volley.newRequestQueue(this);

        //Realizar la petición

        JsonObjectRequest miPeticion;

        //Al completar cargarla

        miPeticion=new JsonObjectRequest(Request.Method.GET, miUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Obtener los datos que nos retorno la api
                        try {
                            //Cargar el string full name de cada uno de los equipos
                            JSONArray miArreglo = response.getJSONArray("data");//Nombre del arreglo API que se trae
                            List<String> elementos = new ArrayList<>();

                            for (int i = 0; i < miArreglo.length(); i++) {
                                JSONObject miElemento = miArreglo.getJSONObject(i);
                                elementos.add(miElemento.getString("full_name"));
                            }

                            //Mostrar los resultados del RecyclerView

                            RecyclerView rv;
                            rv = findViewById(R.id.rv_elementos);
                            myAdapter = new Adapter(elementos);
                            rv.setAdapter(myAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"Hubo un error");
            }
        });
        miPila.add(miPeticion);
    }
}
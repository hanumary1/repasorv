package com.casallas.repaso_rv;

//Esta clase es para hacer el viewholder, el cajon  de vista

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Element extends RecyclerView.ViewHolder {

    TextView txtElement;//refiere al textview que esta en layout elemento

    //Contructor del recycler
    public Element(@NonNull View itemView) {
        super(itemView);
        txtElement=itemView.findViewById(R.id.txtElement);
    }
}
